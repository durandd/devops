package Controleur;

import java.util.List;

import DAL.I_ProduitDAO;
import DAL.ProduitDAOFactory;
import Metier.Catalogue;
import Metier.I_Catalogue;
import Metier.I_Produit;

public class ControleurProduit {
	
	private I_Catalogue catalogue; 
	private ControleurAchat ctrAchat;
	private ControleurStock ctrStock;
	private I_ProduitDAO tableProduit;
	
	public ControleurProduit(){

		catalogue = new Catalogue();

		tableProduit = ProduitDAOFactory.createProduitDAO(catalogue);
		List<I_Produit> listProduit = tableProduit.findAll();
		if (listProduit != null)
			catalogue.addProduits(listProduit);
		
		ctrAchat = new ControleurAchat(catalogue,tableProduit);
		ctrStock = new ControleurStock(catalogue);
		
	}
	
	public boolean ajouterProduit(String nom, double prix, int qte){
		if (1 < nom.length() && nom.length() <= 20){
			boolean res = catalogue.addProduit(nom, prix, qte);
			if (res)
				res = tableProduit.create(nom,prix,qte);
				if (!res)
					catalogue.removeProduit(nom);
			return res;
		}
		return false;
	}

	public String[] getNomsProduits() {
		return catalogue.getNomProduits();
	}

	public boolean supprimerProduit(String nom) {
		boolean res = catalogue.removeProduit(nom);
		if (res)
			res = tableProduit.delete(nom);
			//if (!res)
				// TODO : ICI
				//catalogue.addProduit(nom,prix,qte);
		return res;
	}

	public ControleurAchat getCtrAchat() {
		return ctrAchat;
	}	
	
	public ControleurStock getCtrStock() {
		return ctrStock;
	}		
	
}
