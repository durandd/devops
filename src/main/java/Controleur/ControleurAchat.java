package Controleur;

import DAL.I_ProduitDAO;
import Metier.I_Catalogue;

public class ControleurAchat {

	private I_Catalogue catalogue;
	private I_ProduitDAO tableProduit;
	
	public ControleurAchat(I_Catalogue cat, I_ProduitDAO tabProd){
		catalogue = cat;
		tableProduit = tabProd;
	}
	
	
	public boolean vendreProduit(String nom, int qte) {
		boolean res =  catalogue.vendreStock(nom, qte);
		if (res)
			tableProduit.update(nom,-qte);
		return res;
		
	}

	public boolean acheterProduit(String nom, int qte) {
		boolean res =  catalogue.acheterStock(nom, qte);
		if (res)
			tableProduit.update(nom,qte);
		return res;
	}
}
