package Metier;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Produit implements I_Produit{

	private String nom;
	private int quantiteStock;
	private double prixUnitaireHT;
	private static final double tauxTVA = 0.2;
	private final static NumberFormat formatter = new DecimalFormat("#0.00");
	
	public Produit(String n, double prixUniHt, int q){
		nom = n.replace('	',' ').trim();
		prixUnitaireHT = prixUniHt;
		quantiteStock = q;
	}
	
	@Override
	public boolean ajouter(int qteAchetee) {
		quantiteStock += qteAchetee;
		return true;
	}

	@Override
	public boolean enlever(int qteVendue) {
		if(quantiteStock - qteVendue < 0){
			return false;
		}
		else{
			quantiteStock -= qteVendue;
			return true;
		}
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public int getQuantite() {
		return quantiteStock;
	}

	@Override
	public double getPrixUnitaireHT() {
		return prixUnitaireHT;
	}

	@Override
	public double getPrixUnitaireTTC() {
		return prixUnitaireHT + prixUnitaireHT*tauxTVA;
	}

	@Override
	public double getPrixStockTTC() {
		return getPrixUnitaireTTC()*quantiteStock;
	}

	@Override
	public String toString() {
		
		String s = nom + " - "
				+ "prix HT : " + formatter.format(prixUnitaireHT) + " € - " 
				+ "prix TTC : " + formatter.format(getPrixUnitaireTTC()) + " € - " 
				+ "quantité en stock : " + quantiteStock;
		
		return s.replace('.', ',');
	}

}
