package Metier;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Catalogue implements I_Catalogue{

	private ArrayList<I_Produit> lesProduits;
	//private static Catalogue instance;

	private final static NumberFormat formatter = new DecimalFormat("#0.00");
	
	public Catalogue(){
		lesProduits = new ArrayList<I_Produit>();
	}
	/*
	public static I_Catalogue getInstance(){
		if (null == instance){
			instance = new Catalogue();
		}
		return instance;
	}
	*/
	
	@Override
	public boolean addProduit(I_Produit produit) {
		if (produit != null
			&& existe(produit.getNom())==-1 
			&& produit.getPrixUnitaireHT() > 0 
			&& produit.getQuantite() >= 0) {
			lesProduits.add(produit);
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public boolean addProduit(String nom, double prix, int qte) {
		I_Produit p = new Produit(nom, prix, qte);
		return addProduit(p);
	}

	@Override
	public int addProduits(List<I_Produit> l) {
		int cpt = 0;
		if (l != null){
			for(int i = 0; i<l.size(); i++){
				if(addProduit(l.get(i))){
					cpt++;
				}
			}
		}
		return cpt;
	}

	@Override
	public boolean removeProduit(String nom) {
		int index = existe(nom);
		if (index >= 0){
			lesProduits.remove(index);
			return true;
		}
		return false;
	}

	@Override
	public boolean acheterStock(String nomProduit, int qteAchetee) {
		int index = existe(nomProduit);
		if(index >= 0 && qteAchetee > 0){
			return lesProduits.get(index).ajouter(qteAchetee);
		}
		return false;
	}

	@Override
	public boolean vendreStock(String nomProduit, int qteVendue) {
		int index = existe(nomProduit);
		if(index >= 0 && qteVendue > 0){
			return lesProduits.get(index).enlever(qteVendue);
		}
		return false;
	}

	@Override
	public String[] getNomProduits() {
		int size = lesProduits.size();
		String[] tab = new String[size];
		for(int i = 0; i <size; i++){
			tab[i] = lesProduits.get(i).getNom();
		}
		Arrays.sort(tab);
		return tab;
	}

	@Override
	public double getMontantTotalTTC() {
		
		
		
		double montant = 0.0;
		
		for(int i = 0; i <lesProduits.size(); i++){
			montant += lesProduits.get(i).getPrixStockTTC();
		}
		
		BigDecimal bd = new BigDecimal(montant);
		
		bd = bd.setScale(2, RoundingMode.HALF_UP);
		
		return bd.doubleValue();
	}

	@Override
	public void clear() {
		lesProduits = new ArrayList<I_Produit>();
	}

	@Override
	public String toString() {
		String res = "";
		for ( int i = 0 ; i < lesProduits.size(); i++){

			res += lesProduits.get(i) + "\n";

		}

		res += "\nMontant total TTC du stock : " + formatter.format(getMontantTotalTTC()) + " €";		

		return res.replace('.', ',');
	}

	private int existe(String nom){
		boolean existe = false;
		int i = 0;
		while(!existe && i<lesProduits.size()){
			if(lesProduits.get(i).getNom().equals(nom)){
				existe = true;
			}
			else {
				i++;
			}
		}
		if (i == lesProduits.size()){
			i= -1;
		}
		return i;
	}

	@Override
	public I_Produit find(String nom) {
		boolean found = false;
		I_Produit produit = null;
		int i = 0;
		while(!found && i<lesProduits.size()){
			if(lesProduits.get(i).getNom().equals(nom)){
				found = true;
				produit = lesProduits.get(i);

			}
			else {
				i++;
			}
		}
		return produit;
	}

}
