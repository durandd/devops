package InterfaceGraphique;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import Controleur.ControleurProduit;

public class FenetreSuppressionProduit extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btSupprimer;
	private JComboBox<String> combo;
	private ControleurProduit cp;
	
	public FenetreSuppressionProduit(ControleurProduit cpr) {
		cp = cpr;
		setTitle("Suppression produit");
		setBounds(500, 500, 200, 105);
		Container contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout());
		btSupprimer = new JButton("Supprimer");

		combo = new JComboBox<String>(cp.getNomsProduits());
		combo.setPreferredSize(new Dimension(100, 20));
		contentPane.add(new JLabel("Produit"));
		contentPane.add(combo);
		contentPane.add(btSupprimer);

		btSupprimer.addActionListener(this);

		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btSupprimer && combo.getSelectedItem()!=null){
			if(!cp.supprimerProduit(combo.getSelectedItem().toString())){
				FenetreErreurValeur.afficherMsgErreur(this);
			}
			else
				this.dispose();
		}
		else
			this.dispose();
	}

}
