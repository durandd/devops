package InterfaceGraphique;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import Controleur.ControleurAchat;
import Controleur.ControleurProduit;

public class FenetreAchat extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btAchat;
	private JTextField txtQuantite;
	private JComboBox<String> combo;
	private ControleurProduit cp;
	private ControleurAchat cpa;
	
	public FenetreAchat(ControleurProduit cpr, ControleurAchat cpar) {
		cp = cpr;
		cpa = cpar;
		setTitle("Achat");
		setBounds(500, 500, 200, 125);
		Container contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout());
		btAchat = new JButton("Achat");
		txtQuantite = new JTextField(5);
		txtQuantite.setText("0");

		combo = new JComboBox<String>(cp.getNomsProduits());
		combo.setPreferredSize(new Dimension(100, 20));
		contentPane.add(new JLabel("Produit"));
		contentPane.add(combo);
		contentPane.add(new JLabel("Quantité achetée"));
		contentPane.add(txtQuantite);
		contentPane.add(btAchat);

		btAchat.addActionListener(this);

		this.setVisible(true);
		
		
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btAchat){
			String nom = combo.getSelectedItem().toString();
			int qte = Integer.parseInt(txtQuantite.getText());
			if(!cpa.acheterProduit(nom, qte)){
				FenetreErreurValeur.afficherMsgErreur(this);
			}
			else{
				this.dispose();
			}
		}
		else
			this.dispose();
		
	}

}
