package InterfaceGraphique;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import Controleur.ControleurAchat;
import Controleur.ControleurProduit;

public class FenetreVente extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btVente;
	private JTextField txtQuantite;
	private JComboBox<String> combo;
	private ControleurProduit cp;
	private ControleurAchat cpa;

	public FenetreVente(ControleurProduit cpr,ControleurAchat cpar) {
		cp = cpr;
		cpa = cpar;
		setTitle("Vente");
		setBounds(500, 500, 200, 125);
		Container contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout());
		btVente = new JButton("Vente");
		txtQuantite = new JTextField(5);
		txtQuantite.setText("0");

		combo = new JComboBox<String>(cp.getNomsProduits());
		combo.setPreferredSize(new Dimension(100, 20));
		contentPane.add(new JLabel("Produit"));
		contentPane.add(combo);
		contentPane.add(new JLabel("Quantité vendue"));
		contentPane.add(txtQuantite);
		contentPane.add(btVente);

		btVente.addActionListener(this);
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btVente){
			String nom = combo.getSelectedItem().toString();
			int qte = Integer.parseInt(txtQuantite.getText());
			if(!cpa.vendreProduit(nom, qte)){
				FenetreErreurValeur.afficherMsgErreur(this);
			}
			else
				this.dispose();
		}
		else
			this.dispose();
	}

}
