package InterfaceGraphique;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FenetreErreurValeur{

	public static void afficherMsgErreur(JFrame frame){
		JOptionPane.showMessageDialog(frame,
			    "valeur incorrecte",
			    "Erreur",
			    JOptionPane.ERROR_MESSAGE);	
	}

}
