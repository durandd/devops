package DAL;

import java.util.List;

import Metier.I_Catalogue;
import Metier.I_Produit;
import Metier.Produit;

public class AdaptateurProduitDAOXML implements I_ProduitDAO {

	private ProduitDAOXML dao;
	private I_Catalogue catalogue;
	
	public AdaptateurProduitDAOXML(I_Catalogue c){
		dao = new ProduitDAOXML();
		catalogue = c;
	}
	
	@Override
	public boolean create(String nom, double prix, int qte) {
		I_Produit p = new Produit(nom,prix,qte);
		return dao.creer(p);
	}

	@Override
	public boolean delete(String nom) {
		List<I_Produit> listProduit = dao.lireTous();
		I_Produit p = null;
		for (int i = 0; i < listProduit.size(); i++){
			if( listProduit.get(i).getNom().equals(nom))
				p = listProduit.get(i);
		}
		if (p != null)
			return dao.supprimer(p);
		else
			return false;
	}

	@Override
	public boolean update(String nom, int qte) {
		I_Produit produit = catalogue.find(nom);
		return dao.maj(produit);
	}

	@Override
	public List<I_Produit> findAll() {
		return dao.lireTous();
	}

	@Override
	public void deconnexion() {
	}

}
