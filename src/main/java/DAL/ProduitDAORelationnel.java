package DAL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import Metier.I_Produit;
import Metier.Produit;

public class ProduitDAORelationnel implements I_ProduitDAO {

	private Connection cn;
	private Statement st;
	private PreparedStatement pst;
	private CallableStatement cst;
	private ResultSet rs;
	private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String URL = "jdbc:oracle:thin:@gloin:1521:iut";
	private static final String LOGIN = "cottereaul";
	private static final String MDP = "123";

	
	public ProduitDAORelationnel(){
		
		try{
			Class.forName(DRIVER);
			st = null;
			pst = null;
			cst = null;
			cn = DriverManager.getConnection(URL, LOGIN, MDP);
			cn.setAutoCommit(false);
			
		}
		catch (SQLException e){
			e.printStackTrace();
			deconnexion();
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public boolean create(String nom, double prix, int qte){
		String sql = "{call ajoutProduit(?,?,?)}";
		try {
			cst = cn.prepareCall(sql);
			cst.setString(1, nom);
			cst.setDouble(2, prix);
			cst.setInt(3, qte);
			boolean res = cst.execute();
			cn.commit();
			cst.close();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;			
		}
	}
	
	public boolean delete(String nom){
		String sql = "DELETE FROM Produit WHERE nomProduit = ?";
		try {
			pst = cn.prepareStatement(sql);
			pst.setString(1, nom);
			boolean res = pst.execute();
			cn.commit();
			pst.close();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;			
		}
	}
	
	public boolean update(String nom, int qte){
		String sql = "UPDATE Produit set qteProduit = qteProduit + ?  WHERE nomProduit = ?";
		try {
			pst = cn.prepareStatement(sql);
			pst.setInt(1, qte);
			pst.setString(2, nom);
			boolean res = pst.execute();
			cn.commit();
			pst.close();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;			
		}
	}

	public List<I_Produit> findAll(){
		String sql = "Select nomProduit, prixProduit, qteProduit FROM Produit";
		try {
			st = cn.createStatement();
			rs = st.executeQuery(sql);
			
			List<I_Produit> list = new ArrayList<I_Produit>();
			I_Produit p;
			while(rs.next()){
				String nom = rs.getString(1);
				double prix = rs.getDouble(2);
				int qte = rs.getInt(3);
				p = new Produit(nom, prix, qte);
				list.add(p);
			}
			rs.close();
			st.close();
			return list;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;			
		}
	}
	

	public void deconnexion(){
		try{
			if (rs != null){
				rs.close();
			}
			if (st != null){
				st.close();
			}
			if (cn != null){
				cn.close();
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

}

