package DAL;
import java.util.List;
import Metier.I_Produit;
public interface I_ProduitDAO {
	
	public abstract boolean create(String nom, double prix, int qte);
	public abstract boolean delete(String nom);
	public abstract boolean update(String nom, int qte);
	public abstract List<I_Produit> findAll();
	public abstract void deconnexion();

}
