package DAL;

import Metier.I_Catalogue;

public class ProduitDAOFactory {
	
	public static I_ProduitDAO createProduitDAO(I_Catalogue c){
		return new AdaptateurProduitDAOXML(c);
	}
}
